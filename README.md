# tuxracer-touch

Port of Tux Racer to SailfishOS

Based on https://gitlab.com/abmyii/tuxracer-touch

Thank to @abmyii for his work on Ubuntu Touch port! 

|Screenshots||
|:-:|:-:|
|![screenshot1](click/screenshots/1.jpg)|![screenshot1](click/screenshots/2.jpg)|
|![screenshot1](click/screenshots/3.jpg)||

## Build instructions (for Sailfish OS, under Linux or OSX now only)

1. install SailfishSDK
2. just run `makerpm.sh` 
  - if something went wrong, edit path to  `_mer_sdk_path` in script (Path to SailfishSDK instalation directory)

