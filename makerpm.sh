#!/bin/bash
_pwd=`dirname $(readlink -e "$0")`

_app_name="harbour-tuxracer"
_app_long_name=""
_app_icon=""
_mer_sdk_path="$HOME/SailfishOS"
_mer_sfdk="sfdk engine exec"
# _mer_sfdk="ssh -i ${_mer_sdk_path}/vmshare/ssh/private_keys/engine/mersdk mersdk@localhost -p2222"
_mer_target=""

export PATH=${PATH}:$_mer_sdk_path/bin

IFS='' read -r -d '' spec_file_data <<"EOF"
Name:       harbour-tuxracer
Summary:    Tux Racer
Version:    0.9.0
Release:    4
Group:      Games
License:    GPLv2
BuildArch:  $architecture$
URL:        http://tuxracer.sourceforge.net/download.html
#Source0:    %{name}.tar.xz
Requires:   SDL2
BuildRequires: SDL2-devel
Requires:   freetype
BuildRequires: freetype-devel
Requires:   SDL2_mixer
BuildRequires: SDL2_mixer-devel
Requires:   SDL2_image
BuildRequires: SDL2_image-devel
Requires: qt5-qtcore
Requires: qt5-qtsensors
Provides: libGLESv1_CM.so.1
BuildRequires:   pkgconfig(Qt5Core)
BuildRequires:   pkgconfig(Qt5Sensors)

%description
Tux Racer lets you take on the role of Tux the Linux Penguin 
as he races down steep, snow-covered mountains. Enter cups 
and compete to win the title! Tux Racer includes a variety 
of options for gameplay, including the ability to race 
courses in fog, at night, and under high winds.

%prep
# % setup harbour-tuxracer
echo "nothing"

%build
cd %{_topdir}/BUILD
make -j4

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}/usr/{bin,share/%{name},share/applications}
cp -r %{_topdir}/BUILD/data/ %{buildroot}/usr/share/%{name}/
cp %{_topdir}/BUILD/tuxracer %{buildroot}/usr/bin/%{name}
cp %{_topdir}/BUILD/%{name}.desktop %{buildroot}/usr/share/applications/%{name}.desktop
cp %{_topdir}/BUILD/click/tuxracer.png %{buildroot}/usr/share/%{name}/%{name}.png

%files
%defattr(644,root,root,-)
%attr(755,root,root) %{_bindir}/%{name}
#%attr(644,root,root)
 %{_datadir}/%{name}
#%attr(644,root,root) 
%{_datadir}/applications/%{name}.desktop

%changelog 
* $date$ Tux Racer Sailfish port
- small fixes of x,y in touch events
- add orientation SDL_hint for SailfishOS
EOF

IFS='' read -r -d '' desktop_file_data <<"EOF"
[Desktop Entry]
Type=Application
X-Nemo-Application-Type=SDL2 
Icon=/usr/share/harbour-tuxracer/harbour-tuxracer.png
Exec=harbour-tuxracer
Name=Tux Racer
Name[en]=Tux Racer
EOF

IFS='' read -r -d '' help_data <<"EOF"
Make RPM script v 1.0.0
    Just build TuxRacer rpm for SailfishOS
EOF

function print_help() 
{
    echo "$help_data"
}

function parse_args() 
{
    local argc=0

    while [ $# -gt 0 ] ; do	
        ((argc++))
        case "$1" in
        "-t" | "--target" )
            shift
            _mer_target="$1"
        ;;
        "-pck" | "--pck" )
            shift
            _pck_file="$1"
        ;;
        "-g" | "--godot" )
            shift 
            _godot_binary="$1"
        ;;
        "-icon" | "--icon" )
            shift
            _app_icon="$1"
        ;;
        "-n" | "--name" )
            shift 
            _app_name="$1"
        ;;
        "-ln" | "--long-name" )
            shift 
            _app_long_name="$1"
        ;;
        "-v" | "--version" )
            shift 
            _version="$1"
        ;;
        "-r" | "--release" )
            shift
            _release="$1"
        ;;
        "-c" | "--changelog" )
        	shift
        	_changelog="$1"
        ;;
        * )
            echo "Unknown parameter $argc: $1"
        ;;
		esac
        shift
    done
    return 0
}

function check_args()
{
    local error_=0
    if [ -z $_pck_file ] ; then 
        echo "You should set path to pck file by --pck <path>"
        error_=1
    fi

    if [ -z $_godot_binary ] ; then 
        echo "You should set path to godot export template binary by -g/--godot <path>"
        error_=1
    fi

    if [ -z $_app_icon ] ; then 
        echo "You should set path to game icon file by --icon <path> (PNG file)"
        error_=1
    fi

    if [ -z "$_app_long_name" ] ; then
        echo "You dont set Appliction Human Readable Name, set app_name"
        _app_long_name="${_app_name}"
    fi

    [ $error_ -ne 0 ] && print_help || echo -n ""

    return $error_
}

function check_mer_target() 
{
    local target_count=0
    local option
    if [ -z "$_mer_target" ] ; then
        echo "Mer target is not setuped, please choose one: "
        local targets="`$_mer_sfdk sb2-config -l`"
        # local binary_suffix=`echo "$_godot_binary"|sed -e "s~.*godot\.sailfish\.opt\.\(arm\|x86\)~\1~g"`
        for target in $targets ; do
            eval option[${target_count}]="$target" && ((target_count++))
            _mer_target="$_mer_target $target"
        done
        # dont use all targets, bacuse we have only for one target binary
        eval option[${target_count}]=\"All targets\"
         # ((target_count++))
        if [ $target_count -eq 1 ] ; then
            _mer_target="${option[0]}" &&  echo "Automatically choose \"$_mer_target\""
        else
            select opt in "${option[@]}" ; do
                if [ -z "$opt" ] ; then
                    echo "Invalid option $REPLY"
                elif [[ $opt == "All targets" ]] ; then
                    echo "You choose all targets."
                    _mer_target="`$_mer_sfdk sb2-config -l`"
                    break
                else
                    echo "You  choose \"$opt\" target."
                    _mer_target=$opt
                    break
                fi
            done
        fi
    fi

    # _mer_target="SailfishOS-3.2.1.20-armv7hl SailfishOS-3.2.1.20-i486"
    # $_mer_sfdk sb2-config -l
    echo $_mer_target
}

function prepare_build_folder() 
{
    check_mer_target
    local current_target=""
    local current_date="`LC_ALL=en_EN.UTF-8 date "+%a %b %d %Y"`"
    local build_root="${_pwd}/buildroot"
    
    for current_target in ${_mer_target} ; do
        # current_target="SailfishOS-3.2.1.20-armv7hl"
        local current_build_root="${build_root}/$current_target"
        local sb2_command="$_mer_sfdk sb2 -t $current_target "
        local arch=`echo $current_target|sed -e 's~.*\(i486\|armv7hl\|aarch64\)~\1~g'`
        echo "Use architecture: $arch"
        echo "Install deps:"
        $sb2_command -R zypper in -y SDL2-devel SDL2_mixer-devel SDL2_image-devel freetype-devel mesa-llvmpipe-libGLESv1-devel
        echo "Current target is \"$current_target\""
        # clear build folder 
        if [ -d ${current_build_root} ] ; then
            echo "Remove old build dir."
            rm -fr ${current_build_root}
        fi

        # create RPM build folders 
        echo "Create directories {SOURCES,BUILD,SPECS} in ${current_build_root}."
        mkdir -p "${current_build_root}"/{SOURCES,BUILD,SPECS,BUILDROOT}
        mkdir -p "${current_build_root}"/BUILDROOT/usr/{bin,share}
        mkdir -p "${current_build_root}"/BUILDROOT/usr/share/{$_app_name,applications}
        # need make normal icons folder like /usr/share/icons/86x86/my_app_game.png
        #${sb2_command} mkdir -p "${current_build_root}"/BUILD/usr/share/icons/{86x86,108x108,128x128,256x256}

        # copy game resources
        echo "Copy game resources to build rpm directory."
        git archive master | tar -x -C  ${current_build_root}/BUILD/
                
        echo "Generate ${_app_name}.desktop file."
        echo "$desktop_file_data">"${current_build_root}"/BUILD/${_app_name}.desktop
        local spec_file_path="${current_build_root}/SPECS/${_app_name}.spec"
        
        echo "Generate ${_app_name}.spec file."
        echo "$spec_file_data"|sed -e "s~\\\$architecture\\\$~${arch}~g" -e "s~\\\$date\\\$~${current_date}~g" >"${spec_file_path}"
        
        echo "Pack all data to RPM file."

        echo "${current_target} ${current_build_root}"
        local log_file_path="${build_root}/rpmbuild_${_app_name}_${current_target}.log"
        #${sb2_command} "rpmbuild --define \"_topdir ${current_build_root}\" -ba \"${current_build_root}/SPECS/${_app_name}.spec\"" &> "${log_file_path}"
        ${sb2_command} rpmbuild --define "_topdir ${current_build_root}" -ba "${current_build_root}/SPECS/${_app_name}.spec" &> "${log_file_path}"
        if [ $? -ne 0 ] ; then
            echo "We have Error! Look in to ${log_file_path}"
        fi
        echo ""
    done
}

# parse_args "$@"
# check_args
# [ $? -ne 0 ] && exit 1
prepare_build_folder
exit $?
